/*
================================================================================
Map header
================================================================================
*/
#pragma once

struct MapTile {
    bool explored;
    MapTile() : explored(false) {}
};

class Map {
public:
    Map();
    ~Map();
    int getMaxRoomSize() const;
    int getMinRoomSize() const;
    int getMapWidth() const;
    int getMapHeight() const;

    void computeFOV();

    bool isWall(int x, int y) const;
    bool isExplored(int x, int y) const;
    bool isVisible(int x, int y) const;
protected:
    MapTile *mapTile;
    TCODMap *fovMap;
    friend class MapListener;

    void createRoom(bool firstRoom, int x1, int y1, int x2, int y2);
    void makeHole(int x1, int y1, int x2, int y2);
private:
    int mapWidth, mapHeight;
    int maxRoomSize, minRoomSize;
};
