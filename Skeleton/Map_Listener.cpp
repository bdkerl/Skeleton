#include "Game.h"

MapListener::MapListener(Map &map)
    : map(map), roomNum(0) {

}

bool MapListener::visitNode(TCODBsp *node, void *userData) {
    if (node->isLeaf()) {
        int x(0), y(0), width(0), height(0);
        TCODRandom *rng = TCODRandom::getInstance();
        width = rng->getInt(map.getMinRoomSize(), node->w - 2);
        height = rng->getInt(map.getMinRoomSize(), node->h - 2);
        x = rng->getInt(node->x + 1, node->x + node->w - width - 1);
        y = rng->getInt(node->y + 1, node->y + node->h - height - 1);
        map.createRoom(roomNum == 0, x, y, x + width - 1, y + height - 1);

        if (roomNum != 0) {
            map.makeHole(lastX, lastY, x + width / 2, lastY);
            map.makeHole(x + width / 2, lastY, x + width / 2, y + height / 2);
        }
        lastX = x + width / 2;
        lastY = y + height / 2;
        ++roomNum;
    }
    return true;
}
