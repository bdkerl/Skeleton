#include "Game.h"

Camera::Camera(int playerX, int playerY)
    : cameraX(playerX), cameraY(playerY) {
}

/*
========================================
updateCamera()
 Makes the screen appear to move as the player does.
 Allowing maps to be larger than the game screen.
========================================
*/
void Camera::updateCamera() {
    static int viewX(30), viewY(50), originX(0), originY(0), endX(0), endY(0);
    // NW corner of viewport
    originX = (game.player->posX > viewX / 2 - 1 ? game.player->posX - viewX / 2 : 0);
    originY = (game.player->posY > viewX / 2 - 1 ? game.player->posY - viewY / 2 : 0);

    // Let's not go outside the map constraints
    originX > game.gameMap->getMapWidth() - viewX ? originX = game.gameMap->getMapWidth() - viewX : originX;
    originY > game.gameMap->getMapHeight() - viewY ? originY = game.gameMap->getMapHeight() - viewY : originY;
    originX < 0 ? originX = 0 : originX;
    originY < 0 ? originY = 0 : originY;

    // SE corner of viewport
    endX = originX + viewX;
    endY = originY + viewY;
    TCODConsole::root->clear();
    for (int x = originX; x < endX; ++x) {
        for (int y = originY; y < endY; ++y) {
            if (game.gameMap->isVisible(x, y)) {
                TCODConsole::root->putChar(x - originX, y - originY, game.gameMap->isWall(x, y) ? '#' : '.');
            } else if (game.gameMap->isExplored(x, y)) {
                TCODConsole::root->putChar(x - originX, y - originY, game.gameMap->isWall(x, y) ? '|' : '`');
            }
        }
    }

    TCODConsole::root->putChar(game.player->posX - originX, game.player->posY - originY, '@');
    TCODConsole::root->print(40, 40, "Player %d %d", game.player->posX, game.player->posY);
    TCODConsole::root->print(40, 42, "Camera %d %d", originX, originY);
    TCODConsole::flush();
}
