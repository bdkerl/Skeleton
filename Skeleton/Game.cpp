#include "Game.h"

Game::Game()
    : fovRadius(10), computeFOV(true), consoleWidth(80), consoleHeight(50) {
    TCODConsole::root->setCustomFont("skeleton.png", TCOD_FONT_LAYOUT_ASCII_INROW);
    TCODConsole::initRoot(consoleWidth, consoleHeight, "Skeleton", false);
    player = new Player();
    gameMap = new Map();
    camera = new Camera(player->posX, player->posY);
}

Game::~Game() {
    delete player;
    delete gameMap;
    delete camera;
}

bool Game::update()
{
    gameMap->computeFOV();
    camera->updateCamera();
    TCOD_key_t key = TCODConsole::waitForKeypress(true);
    if (key.pressed) {
        switch (key.c) {
        case 'k':
            if (!gameMap->isWall(player->posX, player->posY - 1)) {
                --player->posY;
            }
            break;
        case 'j':
            if (!gameMap->isWall(player->posX, player->posY + 1)) {
                ++player->posY;
            }
            break;
        case 'l':
            if (!gameMap->isWall(player->posX + 1, player->posY)) {
                ++player->posX;
            }
            break;
        case 'h':
            if (!gameMap->isWall(player->posX - 1, player->posY)) {
                --player->posX;
            }
            break;
        case 'q':
            return false;
        default:
            break;
        }
    }
    return true;
}
