/*
================================================================================
Camera Header
================================================================================
*/
#pragma once

class Camera {
public:
    Camera(int playerX, int playerY);
    void updateCamera();
private:
    int cameraX, cameraY;
};
