﻿#include "Game.h"

Game game;
int main() {
    while (!TCODConsole::isWindowClosed()) {
        if (!game.update()) {
            break;
        }
    }
    return 0;
}
