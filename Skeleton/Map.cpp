#include "Game.h"

Map::Map()
    : mapWidth(80), mapHeight(80), maxRoomSize(10), minRoomSize(6) {
    mapTile = new MapTile[mapWidth * mapHeight];
    fovMap = new TCODMap(mapWidth, mapHeight);

    TCODBsp mapTree(0, 0, mapWidth, mapHeight);
    mapTree.splitRecursive(NULL, 8, maxRoomSize, maxRoomSize, 1, 1);
    MapListener listener(*this);
    mapTree.traverseInvertedLevelOrder(&listener, NULL);
}

Map::~Map() {
    delete[] mapTile;
    delete fovMap;
}

int Map::getMaxRoomSize() const {
    return this->maxRoomSize;
}

int Map::getMinRoomSize() const {
    return this->minRoomSize;
}

int Map::getMapWidth() const {
    return this->mapWidth;
}

int Map::getMapHeight() const {
    return this->mapHeight;
}

void Map::computeFOV() {
    fovMap->computeFov(game.player->posX, game.player->posY, game.fovRadius);
}

bool Map::isWall(int x, int y) const {
    return !fovMap->isWalkable(x, y);
}

bool Map::isExplored(int x, int y) const {
    return mapTile[x + y * mapWidth].explored;
}

bool Map::isVisible(int x, int y) const {
    if (fovMap->isInFov(x, y)) {
        mapTile[x + y * mapWidth].explored = true;
        return true;
    }
    return false;
}

void Map::createRoom(bool firstRoom, int x1, int y1, int x2, int y2) {
    makeHole(x1, y1, x2, y2);
    if (firstRoom) {
        game.player->posX = (x1 + x2) / 2;
        game.player->posY = (y1 + y2) / 2;
    }
}

void Map::makeHole(int x1, int y1, int x2, int y2) {
    if (x2 < x1) {
        int temp = x2;
        x2 = x1;
        x1 = temp;
    }
    if (y2 < y1) {
        int temp = y2;
        y2 = y1;
        y1 = temp;
    }
    for (int tileX = x1; tileX <= x2; ++tileX) {
        for (int tileY = y1; tileY <= y2; ++tileY) {
            fovMap->setProperties(tileX, tileY, true, true);
        }
    }
}
