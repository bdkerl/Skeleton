/*
================================================================================
Map Listener for BSP generation Header
================================================================================
*/
#pragma once
#include "Game.h"

class MapListener : public ITCODBspCallback {
public:
    MapListener(Map &map);
    bool visitNode(TCODBsp *node, void *userData);
private:
    Map &map;
    int roomNum;
    int lastX, lastY;
};
