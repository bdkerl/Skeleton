/*
================================================================================
Game logic header
================================================================================
*/
#pragma once
#include <libtcod.hpp>
#include "Camera.h"
#include "Map.h"
#include "Map_Listener.h"
#include "Player.h"

class Game {
public:
    Game();
    ~Game();

    int consoleWidth, consoleHeight;
    int fovRadius;

    bool update();

    Camera *camera;
    Player *player;
    Map *gameMap;

private:
    bool computeFOV;
};

extern Game game;
